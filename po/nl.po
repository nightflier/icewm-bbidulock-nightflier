# Dutch messages for IceWM
# Copyright (C) 2003 Free Software Foundation, Inc.
# Ton Kersten <TonK@Wanadoo.nl>, 2003-2005
#
msgid	""
msgstr	"Project-Id-Version: icewm 1.2.23\n"
	"Report-Msgid-Bugs-To: \n"
	"POT-Creation-Date: 2006-12-17 19:13+0100\n"
	"PO-Revision-Date:  2005-10-07 10:20+0200\n"
	"Last-Translator: Ton Kersten <TonK@Wanadoo.nl>\n"
	"Language-Team: Dutch\n"
	"MIME-Version: 1.0\n"
	"Content-Type: text/plain; charset=ISO-8859-1\n"
	"Content-Transfer-Encoding: 8bit\n"

msgid	" - Power"
msgstr	" - Voeding"

#. /            if (!prettyClock) strcat(s, " ");
msgid	"P"
msgstr	"E"

#, c-format
msgid	" - Charging"
msgstr	" - Opladen"

msgid	"C"
msgstr	"C"

msgid	"CPU Load: "
msgstr	"CPU Belasting:"

msgid	" processes."
msgstr	" processen"

#, c-format
msgid	"Invalid mailbox protocol: \"%s\""
msgstr	"Onbekend E-mail protocol: �%s�"

#, c-format
msgid	"Invalid mailbox path: \"%s\""
msgstr	"Ongeldig mailbox pad: �%s�"

#, c-format
msgid	"Using MailBox \"%s\"\n"
msgstr	"Gebruikt postbus: �%s�\n"

msgid	"Error checking mailbox."
msgstr	"Probleem bij het controleren van het postbus"

#, c-format
msgid	"%ld mail message."
msgstr	"%ld bericht."

#, c-format
msgid	"%ld mail messages."
msgstr	"%ld berichten."

#, fuzzy, c-format
msgid	"Interface %s:\n"
	"  Current rate (in/out):\t%li %s/%li %s\n"
	"  Current average (in/out):\t%lli %s/%lli %s\n"
	"  Total average (in/out):\t%li %s/%li %s\n"
	"  Transferred (in/out):\t%lli %s/%lli %s\n"
	"  Online time:\t%ld:%02ld:%02ld%s%s"
msgstr	"Interface %s:\n"
	"  Huidige   (in/uit):\t%d %s/%d %s\n"
	"  Gemiddeld (in/uit):\t%d %s/%d %s\n"
	"  Totaal    (in/uit):\t%d %s/%d %s\n"
	"  Online-tijd:\t%d:%02d:%02d%s%s"

msgid	"\n"
	"  Caller id:\t"
msgstr	"\n"
	"  Beller id:\t"

msgid	"Workspace: "
msgstr	"Werkplaats: "

msgid	"Back"
msgstr	"Terug"

msgid	"Alt+Left"
msgstr	"Alt+Links"

msgid	"Forward"
msgstr	"Verder"

msgid	"Alt+Right"
msgstr	"Alt+Rechts"

msgid	"Previous"
msgstr	"Terug"

msgid	"Next"
msgstr	"Volgende"

msgid	"Contents"
msgstr	"Inhoudsopgave"

msgid	"Index"
msgstr	"Index"

#. fCloseButton->setWinGravity(NorthEastGravity);
msgid	"Close"
msgstr	"Sluiten"

msgid	"Ctrl+Q"
msgstr	"Ctrl+Q"

#, c-format
msgid	"Usage: %s FILENAME\n"
	"\n"
	"A very simple HTML browser displaying the document specified by "
	"FILENAME.\n"
	"\n"
msgstr	"Syntax: %s BESTANDSNAAM\n"
	"\n"
	"Een eenvoudige HTML-Browser om BESTANDSNAAM te bekijken.\n"
	"\n"

#, c-format
msgid	"Invalid path: %s\n"
msgstr	"Ongeldig pad: %s\n"

msgid	"Invalid path: "
msgstr	"Ongeldig pad: "

msgid	"List View"
msgstr	"Lijst"

msgid	"Icon View"
msgstr	"Pictogrammen"

msgid	"Open"
msgstr	"Openen"

msgid	"Undo"
msgstr	"Ongedaan maken"

msgid	"Ctrl+Z"
msgstr	"Ctrl+Z"

msgid	"New"
msgstr	"Nieuw"

msgid	"Ctrl+N"
msgstr	"Ctrl+N"

msgid	"Restart"
msgstr	"Herstart"

msgid	"Ctrl+R"
msgstr	"Ctrl+R"

#. !!! fix
msgid	"Same Game"
msgstr	"Hetzelfde spel"

#. ****************************************************************************
#. ****************************************************************************
#, c-format
msgid	"Action `%s' requires at least %d arguments."
msgstr	"Actie `%s' heeft minimaal %d argumenten nodig."

#, fuzzy, c-format
msgid	"Invalid expression: `%s'"
msgstr	"Ongeldig argument: �%s�."

#, c-format
msgid	"Named symbols of the domain `%s' (numeric range: %ld-%ld):\n"
msgstr	"Genoemde symbolen van het domein �%s� (Bereik: %ld-%ld):\n"

#, c-format
msgid	"Invalid workspace name: `%s'"
msgstr	"Ongeldig werkplaatsnummer: �%s�"

#, c-format
msgid	"Workspace out of range: %d"
msgstr	"Werkplaats buiten het bereik: %d"

#, fuzzy, c-format
msgid	"Usage: %s [OPTIONS] ACTIONS\n"
	"\n"
	"Options:\n"
	"  -display DISPLAY            Connects to the X server specified by "
	"DISPLAY.\n"
	"                              Default: $DISPLAY or :0.0 when not "
	"set.\n"
	"  -window WINDOW_ID           Specifies the window to manipulate. "
	"Special\n"
	"                              identifiers are `root' for the root "
	"window and\n"
	"                              `focus' for the currently focused "
	"window.\n"
	"  -class WM_CLASS             Window management class of the window"
	"(s) to\n"
	"                              manipulate. If WM_CLASS contains a "
	"period, only\n"
	"                              windows with exactly the same WM_CLASS "
	"property\n"
	"                              are matched. If there is no period, "
	"windows of\n"
	"                              the same class and windows of the same "
	"instance\n"
	"                              (aka. `-name') are selected.\n"
	"\n"
	"Actions:\n"
	"  setIconTitle   TITLE        Set the icon title.\n"
	"  setWindowTitle TITLE        Set the window title.\n"
	"  setGeometry    geometry     Set the window geometry\n"
	"  setState       MASK STATE   Set the GNOME window state to STATE.\n"
	"                              Only the bits selected by MASK are "
	"affected.\n"
	"                              STATE and MASK are expressions of the "
	"domain\n"
	"                              `GNOME window state'.\n"
	"  toggleState    STATE        Toggle the GNOME window state bits "
	"specified by\n"
	"                              the STATE expression.\n"
	"  setHints       HINTS        Set the GNOME window hints to HINTS.\n"
	"  setLayer       LAYER        Moves the window to another GNOME "
	"window layer.\n"
	"  setWorkspace   WORKSPACE    Moves the window to another workspace. "
	"Select\n"
	"                              the root window to change the current "
	"workspace.\n"
	"  listWorkspaces              Lists the names of all workspaces.\n"
	"  setTrayOption  TRAYOPTION   Set the IceWM tray option hint.\n"
	"\n"
	"Expressions:\n"
	"  Expressions are list of symbols of one domain concatenated by `+' "
	"or `|':\n"
	"\n"
	"  EXPRESSION ::= SYMBOL | EXPRESSION ( `+' | `|' ) SYMBOL\n"
	"\n"
msgstr	"Syntax: %s [OPTIES] ACTIES\n"
	"\n"
	"Opties:\n"
	"  -display DISPLAY            Met de door DISPLAY gedefinieerde X-"
	"Server\n"
	"                              verbinden.\n"
	"                              Voorbeeld: $DISPLAY of :0.0 als "
	"DISPLAY niet gezet is.\n"
	"  -window VENSTER_ID          Het te manipuleren venster. "
	"Bijzondere\n"
	"                              ID's zijn �root� voor de hele "
	"werkplaats en �focus�\n"
	"                              voor het venster dat nu de focus "
	"heeft.\n"
	"\n"
	"Acties:\n"
	"  setIconTitle TITEL          Zet de icoon titel.\n"
	"  setWindowTitle TITEL        Zet de venster titel.\n"
	"  setState MASKER TOESTAND    Zet de Gnome venster instelling naar "
	"TOESTAND\n"
	"                              Alleen de door MASKER gekozen bits "
	"worden\n"
	"                              veranderd. TOESTAND en MASKER zijn "
	"uitdrukkingen\n"
	"\t                       uit het �GNOME-Venstertoestand� domein.\n"
	"  toggleState TOESTAND        Zet de Gnome venster instelling naar "
	"TOESTAND\n"
	"  setHints OMSCHRIJVING       Zet de Gnome venster omschrijving.\n"
	"  setLayer DESK               Plaatst het venster op een andere "
	"desktop.\n"
	"  setWorkspace WERKPLAATS     Plaatst het venster op een andere "
	"werkplaats.\n"
	"                              Bij de keuze voor een andere "
	"werkplaats wordt de\n"
	"                              huidige werkplaats gewisseld.\n"
	"  listWorkspaces              Toont een lijst van alle "
	"werkplaatsen.\n"
	"  setTrayOption TRAYOPTIE     Stelt de IceWM-Trayoptie in.\n"
	"\n"
	"Expressies:\n"
	"  Expressies zijn reeksen symbolen uit hetzelfde domein, die door "
	"het  plusteken �+� of een vertikale streep �|� verbonden zijn:\n"
	"\n"
	"  EXPRESSIE ::= SYMBOOL | EXPRESSIE ( `+' | `|' ) SYMBOOL\n"
	"\n"

msgid	"GNOME window state"
msgstr	"GNOME-Venster status"

msgid	"GNOME window hint"
msgstr	"GNOME-Venster omschrijving"

msgid	"GNOME window layer"
msgstr	"GNOME venster laag"

msgid	"IceWM tray option"
msgstr	"IceWM tray optie"

msgid	"Usage error: "
msgstr	"Syntaxfout: %d"

#, c-format
msgid	"Invalid argument: `%s'."
msgstr	"Ongeldig argument: �%s�."

msgid	"No actions specified."
msgstr	"Geen aktie aangegeven."

#. ====== connect to X11 ===
#, c-format
msgid	"Can't open display: %s. X must be running and $DISPLAY set."
msgstr	"Kan het display %s niet gebruiken. De X-Server moet werken en \n"
	"de omgevingsvariabele $DISPLAY moet hiernaar verwijzen."

#, c-format
msgid	"Invalid window identifier: `%s'"
msgstr	"Ongeldige venster identificatie: �%s�"

#, c-format
msgid	"workspace #%d: `%s'\n"
msgstr	"Werkplaats #%d: �%s�\n"

#, c-format
msgid	"Unknown action: `%s'"
msgstr	"Onbekende actie: �%s�"

#, c-format
msgid	"Socket error: %d"
msgstr	"Sokjes fout ;-): %d"

#, c-format
msgid	"Playing sample #%d (%s)"
msgstr	"Speel sample #%d (%s)"

#, c-format
msgid	"No such device: %s"
msgstr	"Onbekend apparaat: %s"

#, c-format
msgid	"Can't connect to ESound daemon: %s"
msgstr	"Kan geen verbinding met de ESound-Daemon maken: %s"

msgid	"<none>"
msgstr	"<geen>"

#, c-format
msgid	"Error <%d> while uploading `%s:%s'"
msgstr	"Fout <%d> bij het uploaden van �%s:%s�"

#, c-format
msgid	"Sample <%d> uploaded as `%s:%s'"
msgstr	"Sample <%d> werd als �%s:%s� geladen"

#, c-format
msgid	"Playing sample #%d"
msgstr	"Speel sample #%d"

#, c-format
msgid	"Can't connect to YIFF server: %s"
msgstr	"Kan geen verbinding met YIFF-server maken: %s"

#, c-format
msgid	"Can't change to audio mode `%s'."
msgstr	"Kan niet naar audiomode �%s� wisselen."

#, fuzzy, c-format
msgid	"Audio mode switch detected, initial audio mode `%s' no longer in "
	"effect."
msgstr	"Audiomode wissel gedetecteerd. De oorspronkelijke audiomode �%s� "
	"wordt niet verder gebruikt."

msgid	"Audio mode switch detected, automatic audio mode changing disabled."
msgstr	"Audiomode wissel gedetecteerd. Automatische wissel uitgeschakeld."

#, c-format
msgid	"Overriding previous audio mode `%s'."
msgstr	"Vorige audiomode overruled �%s�."

#, fuzzy, c-format
msgid	"             Usage: %s [OPTION]...\n"
	"             \n"
	"             Plays audio files on GUI events raised by IceWM.\n"
	"             \n"
	"             Options:\n"
	"             \n"
	"             -d, --display=DISPLAY         Display used by IceWM "
	"(default: $DISPLAY).\n"
	"             -s, --sample-dir=DIR          Specifies the directory "
	"which contains\n"
	"             the sound files (ie ~/.icewm/sounds).\n"
	"             -i, --interface=TARGET        Specifies the sound "
	"output target\n"
	"             interface, one of OSS, YIFF, ESD\n"
	"             -D, --device=DEVICE           (OSS only) specifies the "
	"digital signal\n"
	"             processor (default /dev/dsp).\n"
	"             -S, --server=ADDR:PORT     (ESD and YIFF) specifies "
	"server address and\n"
	"             port number (default localhost:16001 for ESD\n"
	"             and localhost:9433 for YIFF).\n"
	"             -m, --audio-mode[=MODE]       (YIFF only) specifies the "
	"Audio mode (leave\n"
	"             blank to get a list).\n"
	"             --audio-mode-auto          (YIFF only) change Audio "
	"mode on the fly to\n"
	"             best match sample's Audio (can cause\n"
	"             problems with other Y clients, overrides\n"
	"             --audio-mode).\n"
	"             \n"
	"             -v, --verbose                 Be verbose (prints out "
	"each sound event to\n"
	"             stdout).\n"
	"             -V, --version                 Prints version "
	"information and exits.\n"
	"             -h, --help                    Prints (this) help screen "
	"and exits.\n"
	"             \n"
	"             Return values:\n"
	"             \n"
	"             0     Success.\n"
	"             1     General error.\n"
	"             2     Command line error.\n"
	"             3     Subsystems error (ie cannot connect to server).\n"
	"\n"
msgstr	"Syntax: %s [OPTION]...\n"
	"\n"
	"Speelt de bij IceWM voorkomende GUI-acties passende "
	"geluidsbestanden.\n"
	"\n"
	"Opties:\n"
	"\n"
	"  -d, --display=DISPLAY         Het door IceWM gebruikte display\n"
	"                                (Standaard: $DISPLAY).\n"
	"  -s, --sample-dir=BESTAND      Een audio bestand   -i, --"
	"interface=DOEL          Het gebruikte audiointerface,\n"
	"                                zoals OSS, YIFF of ESD\n"
	"  -D, --device=GER�T            Alleen OSS: De digitale "
	"signalprocessor\n"
	"                                (Standaard: /dev/dsp).\n"
	"  -S, --server=ADRESE:POORT     ESD en YIFF: Serveradres en -"
	"poortnummer\n"
	"                                (Standard: localhost:16001 f�r ESD\n"
	"                                und localhost:9433 f�r YIFF).\n"
	"  -m, --audio-mode[=MODE]       Alleen YIFF: Audiomode (leeg laten, "
	"om een\n"
	"                                overzicht te krijgen).\n"
	"  --audio-mode-auto             Alleen YIFF: Wisselen van de "
	"audiomode naar behoefte\n"
	"                                (Kan problemen met andere Y-clients\n"
	"                                veroorzaken, overschrijft --audio-"
	"mode).\n"
	"  -v, --verbose                 Wees praatgraag. (Toont alle GUI-"
	"interacties op\n"
	"                                stdout).\n"
	"  -V, --version                 Toont de programma versie.\n"
	"  -h, --help                    Toont deze hulp.\n"
	"\n"
	"Resultaat:\n"
	"\n"
	"  0     Succes.\n"
	"  1     Algemene fout.\n"
	"  2     Commando regel fout.\n"
	"  3     Fout in een subsysteem (In het algemeen geen verbinding met "
	"de server)\n"
	" \n"

msgid	"Multiple sound interfaces given."
msgstr	"Er werden meerdere audiointerfaces aangegeven."

#, c-format
msgid	"Support for the %s interface not compiled."
msgstr	"De ondersteuning voor de %s-interface werd niet meegecompileerd."

#, c-format
msgid	"Unsupported interface: %s."
msgstr	"Niet ondersteunde audiointerface: %s."

#, c-format
msgid	"Received signal %d: Terminating..."
msgstr	"Signaal %d ontvangen: Het programma wordt gestopt..."

#, c-format
msgid	"Received signal %d: Reloading samples..."
msgstr	"Signaal %d ontvangen: Samples worden geactualiseerd..."

msgid	"Hex View"
msgstr	"Hexadecimal"

msgid	"Ctrl+H"
msgstr	"Ctrl+H"

msgid	"Expand Tabs"
msgstr	"Tabstops uitbreiden"

msgid	"Ctrl+T"
msgstr	"Ctrl+T"

msgid	"Wrap Lines"
msgstr	"Lange regels afbreken"

msgid	"Ctrl+W"
msgstr	"Ctrl+W"

msgid	"Usage: icewmbg [ -r | -q ]\n"
	" -r  Restart icewmbg\n"
	" -q  Quit icewmbg\n"
	"Loads desktop background according to preferences file\n"
	" DesktopBackgroundCenter  - Display desktop background centered, not "
	"tiled\n"
	" SupportSemitransparency  - Support for semitransparent terminals\n"
	" DesktopBackgroundColor   - Desktop background color\n"
	" DesktopBackgroundImage   - Desktop background image\n"
	" DesktopTransparencyColor - Color to announce for semi-transparent "
	"windows\n"
	" DesktopTransparencyImage - Image to announce for semi-transparent "
	"windows\n"
msgstr	"Gebruik: icewmbg [ -r | -q ]\n"
	" -r  Herstart icewmbg\n"
	" -q  Stop icewmbg\n"
	"Laad bureaublad achtergrond zoals gedefinieerd in voorkeur bestand\n"
	" DesktopBackgroundCenter  - Toon achtergrond gecentreerd\n"
	" SupportSemitransparency  - Ondersteun semi-transparante "
	"achtergrond\n"
	" DesktopBackgroundColor   - Bureaublad achtergrond kleur\n"
	" DesktopBackgroundImage   - Bureaublad achtergrond afbeelding\n"
	" DesktopTransparencyColor - Kleur voor semi-transparente "
	"achtergrond\n"
	" DesktopTransparencyImage - Afbeelding voor semi-transparente "
	"achtergrond\n"

#, c-format
msgid	"%s: unrecognized option `%s'\n"
	"Try `%s --help' for more information.\n"
msgstr	"%s: Onbekende optie: �%s�\n"
	"Probeer �%s --help� voor meer informatie.\n"

#, c-format
msgid	"Loading image %s failed"
msgstr	"Fout bij het laden van plaatje %s"

#, c-format
msgid	"Loading of pixmap \"%s\" failed: %s"
msgstr	"Fout bij het laden van plaatje �%s�: %s"

msgid	"Usage: icewmhint [class.instance] option arg\n"
msgstr	"Syntax: icewmhint [klasse.instantie] optie argument\n"

#, c-format
msgid	"Out of memory (len=%d)."
msgstr	"Onvoldoende geheugen (len=%d)."

msgid	"Warning: "
msgstr	"Waarschuwing: "

#, c-format
msgid	"Unknown direction in move/resize request: %d"
msgstr	"Onbekende richting in verplaats/schaal verzoek: %d"

#, fuzzy
msgid	"Default"
msgstr	"Verwijderen"

msgid	"(C)"
msgstr	"�"

msgid	"Theme:"
msgstr	"Thema:"

msgid	"Theme Description:"
msgstr	"Thema omschrijving:"

msgid	"Theme Author:"
msgstr	"Thema auteur:"

msgid	"icewm - About"
msgstr	"IceWM - Over"

msgid	"Unable to get current font path."
msgstr	"Kan het lettertype pad niet bepalen."

msgid	"Unexpected format of ICEWM_FONT_PATH property"
msgstr	"Onverwacht formaat van de ICEWM_FONT_PATH eigenschap."

#, c-format
msgid	"Multiple references for gradient \"%s\""
msgstr	"Meerdere verwijzingen naar de gradient �%s�."

#, c-format
msgid	"Unknown gradient name: %s"
msgstr	"Onbekende gradient naam: �%s�"

# OS/2 is dead, but... ;-)
msgid	"_Logout"
msgstr	"_Afmelden"

msgid	"_Cancel logout"
msgstr	"_Afmelden afbreken"

msgid	"Lock _Workstation"
msgstr	"Werkstation _blokkeren"

msgid	"Re_boot"
msgstr	"_Re_boot"

msgid	"Shut_down"
msgstr	"Sto_ppen"

msgid	"Restart _Icewm"
msgstr	"_Icewm Opnieuw starten"

msgid	"Restart _Xterm"
msgstr	"_Xterm opnieuw starten"

msgid	"_Menu"
msgstr	"_Menu"

msgid	"_Above Dock"
msgstr	"_Over de Dock"

msgid	"_Dock"
msgstr	"_Dock"

msgid	"_OnTop"
msgstr	"_BovenIn"

msgid	"_Normal"
msgstr	"_Normaal"

msgid	"_Below"
msgstr	"_Onder"

msgid	"D_esktop"
msgstr	"D_esktop"

msgid	"_Restore"
msgstr	"_Herstellen"

msgid	"_Move"
msgstr	"_Verplaatsen"

msgid	"_Size"
msgstr	"_Grootte veranderen"

msgid	"Mi_nimize"
msgstr	"Mi_nimaliseren"

msgid	"Ma_ximize"
msgstr	"Ma_ximaliseren"

msgid	"_Fullscreen"
msgstr	"_Volledig scherm"

msgid	"_Hide"
msgstr	"Verb_ergen"

msgid	"Roll_up"
msgstr	"Op_rollen"

msgid	"R_aise"
msgstr	"St_ijgen"

msgid	"_Lower"
msgstr	"Da_len"

msgid	"La_yer"
msgstr	"_Laag"

msgid	"Move _To"
msgstr	"Verschuiven naa_r"

msgid	"Occupy _All"
msgstr	"Op _alle werkplaatsen"

msgid	"Limit _Workarea"
msgstr	"_Beperkte werkplaats"

msgid	"Tray _icon"
msgstr	"Tray_icon"

msgid	"_Close"
msgstr	"_Sluiten"

msgid	"_Kill Client"
msgstr	"Programma stoppen"

msgid	"_Window list"
msgstr	"_Window lijst"

#
msgid	"Another window manager already running, exiting..."
msgstr	"Een andere window manager is al aktief. Icewm wordt gestopt...."

#, c-format
msgid	"Could not restart: %s\n"
	"Does $PATH lead to %s?"
msgstr	"De herstart is mislukt: %s\n"
	"Verwijst de �PATH� variable naar de programma directory �%s�?"

#, fuzzy, c-format
msgid	"Usage: %s [OPTIONS]\n"
	"Starts the IceWM window manager.\n"
	"\n"
	"Options:\n"
	"  --display=NAME      NAME of the X server to use.\n"
	"%s  --sync              Synchronize X11 commands.\n"
	"\n"
	"  -c, --config=FILE   Load preferences from FILE.\n"
	"  -t, --theme=FILE    Load theme from FILE.\n"
	"  -n, --no-configure  Ignore preferences file.\n"
	"\n"
	"  -v, --version       Prints version information and exits.\n"
	"  -h, --help          Prints this usage screen and exits.\n"
	"%s  --replace           Replace an existing window manager.\n"
	"  --restart           Don't use this: It's an internal flag.\n"
	"\n"
	"Environment variables:\n"
	"  ICEWM_PRIVCFG=PATH  Directory to use for user private "
	"configuration files,\n"
	"                      \"$HOME/.icewm/\" by default.\n"
	"  DISPLAY=NAME        Name of the X server to use, depends on Xlib "
	"by default.\n"
	"  MAIL=URL            Location of your mailbox. If the schema is "
	"omitted\n"
	"                      the local \"file\" schema is assumed.\n"
	"\n"
	"Visit http://www.icewm.org/ for report bugs, support requests, "
	"comments...\n"
msgstr	"Gebruik: %s [OPTIES]\n"
	"Start de IceWM window manager.\n"
	"\n"
	"Opties:\n"
	"  --display=NAAM      NAAM van de te gebruiken X server.\n"
	"%s  --sync              Synchroniseer X11 commando's.\n"
	"\n"
	"  -c, --config=BESTAND   Laad voorkeuren uit BESTAND.\n"
	"  -t, --theme=BESTAND    Laad thema uit BESTAND.\n"
	"  -n, --no-configure  Negeer voorkeuren bestand.\n"
	"\n"
	"  -v, --version       Toon versie informatie en stop.\n"
	"  -h, --help          Toon deze hulp en stop.\n"
	"%s  --restart           Niet gebruiken. Uitsluitend voor intern "
	"gebruik.\n"
	"\n"
	"Omgevings variabelen:\n"
	"  ICEWM_PRIVCFG=PAD   Directory voor persoonlijke "
	"voorkeurbestanden,\n"
	"                      standaars \"$HOME/.icewm/\".\n"
	"  DISPLAY=NAME        NAAM van de te gebruiker X server, hangt af "
	"van Xlib by default.\n"
	"  MAIL=URL            Lokatie van uw postbus. Als het schema niet is "
	"opgegeven\n"
	"                      wordt aangenomen dat het lokale schema "
	"gebruikt moet worden.\n"
	"\n"
	"Bezoek http://www.icewm.org/ voor fout meldingen, ondersteunings "
	"verzoeken, commentaar...\n"

msgid	"Confirm Logout"
msgstr	"Afmelden bevestigen"

msgid	"Logout will close all active applications.\n"
	"Proceed?"
msgstr	"Bij het afmelden worden alle actieve programma's afgesloten.\n"
	"Afmelden?"

#
msgid	"Bad Look name"
msgstr	"Ongeldige 'Kijk-naam' (look-Option)"

msgid	"_Logout..."
msgstr	"_Afmelden..."

msgid	"_Cancel"
msgstr	"Af_breken"

msgid	"_Restart icewm"
msgstr	"IceWM _herstarten"

msgid	"Maximize"
msgstr	"Maximaliseren"

#. fMinimizeButton->setWinGravity(NorthEastGravity);
msgid	"Minimize"
msgstr	"Minimaliseren"

#. fHideButton->setWinGravity(NorthEastGravity);
msgid	"Hide"
msgstr	"Verbergen"

#. fRollupButton->setWinGravity(NorthEastGravity);
msgid	"Rollup"
msgstr	"Oprollen"

#
#. fDepthButton->setWinGravity(NorthEastGravity);
msgid	"Raise/Lower"
msgstr	"Stijgen/Dalen"

#
msgid	"Kill Client: "
msgstr	"Stoppen van: "

msgid	"WARNING! All unsaved changes will be lost when\n"
	"this client is killed. Do you wish to proceed?"
msgstr	"PAS OP! Alle niet opgeslagen veranderingen gaan verloren\n"
	"als u dit programma stopt!\n"
	"Wilt u doorgaan?"

msgid	"Restore"
msgstr	"Herstellen"

msgid	"Rolldown"
msgstr	"Afrollen"

#, c-format
msgid	"Error in window option: %s"
msgstr	"Foutieve window optie: %s"

#, c-format
msgid	"Unknown window option: %s"
msgstr	"Onbekende window optie: %s"

msgid	"Syntax error in window options"
msgstr	"Syntaxfout in window optie"

msgid	"Out of memory for window options"
msgstr	"Onvoldoende geheugen voor de window opties"

msgid	"Missing command argument"
msgstr	"Onvoldoende argumenten"

#, c-format
msgid	"Bad argument %d"
msgstr	"Ongeldig argument: %d"

#, c-format
msgid	"Error at prog %s"
msgstr	"Fout bij programma %s"

#, c-format
msgid	"Unexepected keyword: %s"
msgstr	"Onverwacht woord: %s"

#, c-format
msgid	"Error at key %s"
msgstr	"Fout bij toets %s"

#. /    if (programs->itemCount() > 0)
msgid	"Programs"
msgstr	"Programma's"

msgid	"_Windows"
msgstr	"_Windows"

msgid	"_Run..."
msgstr	"Uit_voeren..."

msgid	"_About"
msgstr	"_Over"

msgid	"_Help"
msgstr	"_Help"

msgid	"_Themes"
msgstr	"_Thema's"

#, c-format
msgid	"Session Manager: Unknown line %s"
msgstr	"Sessie manager: Onbekende regel %s"

msgid	"Task Bar"
msgstr	"Taak lijst"

msgid	"Tile _Vertically"
msgstr	"_Vertikaal uitlijnen"

msgid	"T_ile Horizontally"
msgstr	"_Horizontaal uitlijnen"

msgid	"Ca_scade"
msgstr	"_Overlappend uitlijnen"

msgid	"_Arrange"
msgstr	"_Optimaliseren"

msgid	"_Minimize All"
msgstr	"Alles _minimaliseren"

msgid	"_Hide All"
msgstr	"Alles _verstoppen"

msgid	"_Undo"
msgstr	"_Ongedaan"

msgid	"Arrange _Icons"
msgstr	"_Pictogrammen opruimen"

msgid	"_Refresh"
msgstr	"_Verversen"

msgid	"_License"
msgstr	"_Licentie"

msgid	"Favorite applications"
msgstr	"Favoriete programma's"

msgid	"Window list menu"
msgstr	"Window lijst menu"

#, fuzzy
msgid	"Show Desktop"
msgstr	"D_esktop"

#, fuzzy
msgid	"All Workspaces"
msgstr	"Werkplaats: "

#, fuzzy
msgid	"Del"
msgstr	"Verwijderen"

msgid	"_Terminate Process"
msgstr	"Programma stoppen"

msgid	"Kill _Process"
msgstr	"Programma stoppen"

msgid	"_Show"
msgstr	"_Tonen"

msgid	"_Minimize"
msgstr	"Mi_nimaliseren"

msgid	"Window list"
msgstr	"Window lijst"

#, c-format
msgid	"%lu. Workspace %-.32s"
msgstr	"%lu. Werkplaats %-.32s"

#, c-format
msgid	"Unrecognized option: %s\n"
msgstr	"Onbekende optie: %s\n"

#. pos
#, c-format
msgid	"Unrecognized argument: %s\n"
msgstr	"Onbekend argument: %s\n"

#, c-format
msgid	"Argument required for %s switch"
msgstr	"De %s-optie heeft een argument nodig."

#, c-format
msgid	"Unknown key name %s in %s"
msgstr	"Onbekende toets %s in %s"

#, c-format
msgid	"Bad argument: %s for %s"
msgstr	"Ongeldig argument: %s voor %s"

#, c-format
msgid	"Bad option: %s"
msgstr	"Ongeldige optie: %s"

#, c-format
msgid	"Loading of pixmap \"%s\" failed"
msgstr	"Laden van afbeelding �%s� mislukt"

#, c-format
msgid	"Invalid cursor pixmap: \"%s\" contains too much unique colors"
msgstr	"Ongeldige cursorpixmap: �%s� bevat te veel unieke kleuren"

#, c-format
msgid	"BUG? Imlib was able to read \"%s\""
msgstr	"FOUT? Imlib was in staat �%s� te lezen"

#, c-format
msgid	"BUG? Malformed XPM header but Imlib was able to parse \"%s\""
msgstr	"BUG? Ongeldige XPM-Header (Imlib heeft het bestand �%s� toch gelezen)"

#, c-format
msgid	"BUG? Unexpected end of XPM file but Imlib was able to parse \"%s\""
msgstr	"BUG? Onverwacht einde van het XPM-bestand (Imlib heeft het bestand �%"
	"s� toch gelezen)"

#, c-format
msgid	"BUG? Unexpected characted but Imlib was able to parse \"%s\""
msgstr	"BUG? Onverwacht teken (Imlib heeft het bestand �%s� toch gelezen)"

#, c-format
msgid	"Could not load font \"%s\"."
msgstr	"Het lettertype �%s� kon niet geladen worden."

#, c-format
msgid	"Loading of fallback font \"%s\" failed."
msgstr	"Terugval op lettertype �%s� is mislukt."

#, c-format
msgid	"Could not load fontset \"%s\"."
msgstr	"Lettertypeset �%s� kon niet geladen worden."

#, c-format
msgid	"Missing codesets for fontset \"%s\":"
msgstr	"Missende codeset in de lettertpeset �%s�:"

#, c-format
msgid	"Out of memory for pixmap \"%s\""
msgstr	"Onvoldoende geheugen voor afbeelding �%s�"

#, c-format
msgid	"Loading of image \"%s\" failed"
msgstr	"Laden van de afbeelding �%s� mislukt"

msgid	"Imlib: Acquisition of X pixmap failed"
msgstr	"Imlib: Overname van de X Pixmap is mislukt"

msgid	"Imlib: Imlib image to X pixmap mapping failed"
msgstr	"Imlib: Afbeelding van het Imlib-beeld op een X-Pixmap is mislukt"

msgid	"Cu_t"
msgstr	"Kn_ippen"

msgid	"Ctrl+X"
msgstr	"Ctrl+X"

msgid	"_Copy"
msgstr	"_Kopieren"

msgid	"Ctrl+C"
msgstr	"Ctrl+C"

msgid	"_Paste"
msgstr	"_Plakken"

msgid	"Ctrl+V"
msgstr	"Ctrl+V"

msgid	"Paste _Selection"
msgstr	"Selectie _plakken"

msgid	"Select _All"
msgstr	"_Alles selecteren"

msgid	"Ctrl+A"
msgstr	"Ctrl+A"

#. || False == XSupportsLocale()
msgid	"Locale not supported by C library. Falling back to 'C' locale'."
msgstr	"Lokaliteit wordt niet ondersteund. 'C' lokaliteit wordt gebruikt."

msgid	"Failed to determinate the current locale's codeset. Assuming ISO-"
	"8859-1.\n"
msgstr	"Kom huidige lokaliteit niet bepalen. Ga uit van ISO-8859-1.\n"

#, c-format
msgid	"iconv doesn't supply (sufficient) %s to %s converters."
msgstr	"iconv heeft (onvoldoende) %s naar %s-vertalers."

#, c-format
msgid	"Invalid multibyte string \"%s\": %s"
msgstr	"Ongeldige Multibyte-string �%s�: %s"

#, fuzzy
msgid	"program label expected"
msgstr	"Scheidingsteken verwacht"

msgid	"icon name expected"
msgstr	"icoon naam verwacht"

msgid	"window management class expected"
msgstr	"venster beheer klasse verwacht"

#, fuzzy
msgid	"menu caption expected"
msgstr	"Scheidingsteken verwacht"

#, fuzzy
msgid	"opening curly expected"
msgstr	"Sleutelwoord verwacht"

#, fuzzy
msgid	"action name expected"
msgstr	"Scheidingsteken verwacht"

#, fuzzy
msgid	"unknown action"
msgstr	"Onbekende actie: �%s�"

msgid	"OK"
msgstr	"OK"

msgid	"Cancel"
msgstr	"Niet afmelden"

#, c-format
msgid	"Failed to open %s: %s"
msgstr	"Kom %s niet openen: %s"

#, c-format
msgid	"Failed to execute %s: %s"
msgstr	"Kon %s niet uitvoeren: %s"

#, c-format
msgid	"Failed to create child process: %s"
msgstr	"Kon kind proces niet creeeren: %s"

#, c-format
msgid	"Not a regular file: %s"
msgstr	"Is geen standaard bestand: %s"

msgid	"Pair of hexadecimal digits expected"
msgstr	"Paar van hexadecimalen tekens verwacht"

msgid	"Unexpected identifier"
msgstr	"Onverwacht sleutelwoord"

msgid	"Identifier expected"
msgstr	"Sleutelwoord verwacht"

msgid	"Separator expected"
msgstr	"Scheidingsteken verwacht"

#, fuzzy
msgid	"Invalid token"
msgstr	"Ongeldig pad: "

#, c-format
msgid	"Out of memory for pixel map %s"
msgstr	"Onvoldoende geheugen voor pixmap %s"

#, c-format
msgid	"Could not find pixel map %s"
msgstr	"Pixmap %s niet gevonden"

#, c-format
msgid	"Out of memory for RGB pixel buffer %s"
msgstr	"Onvoldoende geheugen voor RGB pixel buffer �%s�"

#, c-format
msgid	"Could not find RGB pixel buffer %s"
msgstr	"RGB pixel buffer �%s� niet gevonden"

#, c-format
msgid	"Using fallback mechanism to convert pixels (depth: %d; masks (red/"
	"green/blue): %0*x/%0*x/%0*x)"
msgstr	"Gebruik terugval mechanisme om pixels te converteren.(Kleurdiepte: %"
	"d, Masker (Rood/Groen/Blauw): %0*x/%0*x/%0*x)"

#, c-format
msgid	"%s:%d: %d bit visuals are not supported (yet)"
msgstr	"%s:%d: %d-Bit-Visuals worden (momenteel) niet ondersteunt"

msgid	"$USER or $LOGNAME not set?"
msgstr	"Zijn de variabelen $USER of $LOGNAME gezet?"

#, c-format
msgid	"\"%s\" doesn't describe a common internet scheme"
msgstr	"�%s� voldoet niet aan de Common Internet Scheme Syntax"

#, c-format
msgid	"\"%s\" contains no scheme description"
msgstr	"�%s� bevat geen schema beschrijving"

#, c-format
msgid	"Not a hexadecimal number: %c%c (in \"%s\")"
msgstr	"Geen hexadecimaal cijfer: %c%c (in �%s�)"

#~ msgid	"/proc/apm - unknown format (%d)"
#~ msgstr	"/proc/apm - Onbekend formaat (%d)"

#~ msgid	"stat:\tuser = %i, nice = %i, sys = %i, idle = %i"
#~ msgstr	"status:\tuser = %i, nice = %i, sys = %i, idle = %i "

#~ msgid	"bars:\tuser = %i, nice = %i, sys = %i (h = %i)\n"
#~ msgstr	"balken:\tuser = %i, nice = %i, sys = %i (h = %i)\n"

#, fuzzy
#~ msgid	"cpu: %d %d %d %d %d %d %d"
#~ msgstr	"CPU: %d %d %d %d"

#~ msgid	"kstat finds too many cpus: should be %d"
#~ msgstr	"kstat meldt te veel CPUs: Het moeten er %d zijn"

#~ msgid	"%s@%d: %s\n"
#~ msgstr	"%s@%d: %s\n"

#~ msgid	"XQueryTree failed for window 0x%x"
#~ msgstr	"XQueryTree is mislukt voor venster 0x%x"

#~ msgid	"Compiled with DEBUG flag. Debugging messages will be printed."
#~ msgstr	"Met DEBUG-vlag gecompileert. Debugging boodschappen worden "
#~ 	"getoont."

#~ msgid	"_No icon"
#~ msgstr	"_Geen icon"

#~ msgid	"_Minimized"
#~ msgstr	"_Geminimaliseert"

#~ msgid	"_Exclusive"
#~ msgstr	"_Exclusief"

#~ msgid	"X error %s(0x%lX): %s"
#~ msgstr	"X-Protocol fout in %s(0x%lX): %s"

#, fuzzy
#~ msgid	"Forking failed (errno=%d)"
#~ msgstr	"Pipe creatie fout (errno=%d)."

#, fuzzy
#~ msgid	"Failed to create anonymous pipe (errno=%d)."
#~ msgstr	"Pipe creatie fout (errno=%d)."

#~ msgid	"Message Loop: select failed (errno=%d)"
#~ msgstr	"Boodschaplus: select mislukt (errno=%d)"

#, fuzzy
#~ msgid	"Invalid cursor pixmap: \"%s\" contains too many unique colors"
#~ msgstr	"Ongeldige cursorpixmap: �%s� bevat te veel unieke kleuren"

#~ msgid	"Failed to create anonymous pipe: %s"
#~ msgstr	"Kon anonieme 'pipe' niet openen: %s"

#~ msgid	"Failed to duplicate file descriptor: %s"
#~ msgstr	"Kan bestandshendel niet dupliceren: %s"

#~ msgid	"%s:%d: Failed to copy drawable 0x%x to pixel buffer"
#~ msgstr	"%s:%d: Kopieren van 0x%x tekenbuffer mislukt"

#, fuzzy
#~ msgid	"%s:%d: Failed to copy drawable 0x%x to pixel buffer (%d:%d-%"
#~ 	"dx%d"
#~ msgstr	"%s:%d: Kopieren van 0x%x tekenbuffer mislukt"

#~ msgid	"TOO MANY ICE CONNECTIONS -- not supported"
#~ msgstr	"TE VEEL ICE VERBINDINGEN -- niet ondersteund"

#~ msgid	"Session Manager: IceAddConnectionWatch failed."
#~ msgstr	"Sessie manager: IceAddConnectionWatch mislukt."

#~ msgid	"Session Manager: Init error: %s"
#~ msgstr	"Sessie manager: Initialisatie fout: %s"

#~ msgid	"M"
#~ msgstr	"L"

#~ msgid	"CPU Load: %3.2f %3.2f %3.2f, %d processes."
#~ msgstr	"CPU Belasting: %3.2f %3.2f %3.2f, %d processen"

#~ msgid	"# preferences(%s) - generated by genpref\n"
#~ 	"\n"
#~ msgstr	"# Instellingen(%s) - Gegenereerd door genpref\n"
#~ 	"\n"

#~ msgid	"# NOTE: All settings are commented out by default, be sure "
#~ 	"to\n"
#~ 	"#       uncomment them if you change them!\n"
#~ 	"\n"
#~ msgstr	"# Opmerking: Alle instellingen zijn standaard uit-"
#~ 	"gecommentarieerd\n"
#~ 	"#            Verwijder het commentaarteken (#) als u iets wilt "
#~ 	"veranderden\n"
#~ 	"\n"

#~ msgid	"Usage: icewmbg [OPTION]... pixmap1 [pixmap2]...\n"
#~ 	"Changes desktop background on workspace switches.\n"
#~ 	"The first pixmap is used as a default one.\n"
#~ 	"\n"
#~ 	"-s, --semitransparency    Enable support for semi-transparent "
#~ 	"terminals\n"
#~ msgstr	"Syntax: icewmbg [OPTIE]... pixmap1 [pixmap2]...\n"
#~ 	"Veranderd de werkplaats achtergrond bij werkplaats wissel.\n"
#~ 	"Het eerste bestand dient als staandard-Pixmap.\n"
#~ 	"\n"
#~ 	"-s, --semitransparency    Activeert de ondersteuning voor "
#~ 	"semitransparente terminals\n"

#~ msgid	"Window %p has no XA_ICEWM_PID property. Export the "
#~ 	"LD_PRELOAD variable to preload the preice library."
#~ msgstr	"Het window %p heeft geen XA_ICEWM_PID eigenschap. Exporteer "
#~ 	"de variable �LD_PRELOAD�, om de preice-programmabibliothek te "
#~ 	"activeren."

#~ msgid	"Obsolete option: %s"
#~ msgstr	"Verouderde Option: %s"

#~ msgid	"Gnome"
#~ msgstr	"Gnome"

#~ msgid	"Gnome User Apps"
#~ msgstr	"Gnome-Gebruikers menu"

#~ msgid	"KDE"
#~ msgstr	"KDE"

#~ msgid	"Resource allocation for rotated string \"%s\" (%dx%d px) "
#~ 	"failed"
#~ msgstr	"Geen resources voor de roterende tekenreeks �%s� (%dx%d px) "
#~ 	"beschikbaar"

#~ msgid	"_Ignore"
#~ msgstr	"_Negeren"

#~ msgid	"Out of memory: Unable to allocated %d bytes."
#~ msgstr	"Gebrek aan geheugen: Onmogelijk om %d bytes te reserveren."

#~ msgid	"Invalid fonts in fontset definition \"%s\":"
#~ msgstr	"Ongeldig lettertype in de lettertype familie �%s�:"
